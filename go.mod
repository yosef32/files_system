module romano.com

go 1.14

require (
	github.com/golang/protobuf v1.3.4
	go.mongodb.org/mongo-driver v1.3.0
	google.golang.org/grpc v1.27.1
)
