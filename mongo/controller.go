package mongo

import (
	"fmt"
	"log"
	"context"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
    "go.mongodb.org/mongo-driver/mongo/options"
)

type Controller struct {
	client *mongo.Client
	database *mongo.Database
	collection *mongo.Collection
}

func NewController(url string) Controller {

	var newController Controller;

	client, err := mongo.NewClient(options.Client().ApplyURI(url))
	if err != nil {
		log.Fatal(err)
	}
	
	err = client.Connect(context.TODO())
    if err != nil {
        log.Fatal(err)
    }
	fmt.Println("Conncet to db");
	
	newController.client = client;
	
	return newController;

}

func (c *Controller) SetCollection(dbName, collectionName string) {
	c.database = c.client.Database(dbName);
	c.collection = c.database.Collection(collectionName);
}

func (c *Controller) Insert(obj primitive.D) string {
	res, err := c.collection.InsertOne(context.TODO(), obj);

	if err != nil {
		log.Fatal(err)
	}

	oid, ok := res.InsertedID.(primitive.ObjectID);

	if !ok {
		return "";
	}

	return oid.Hex();
}

func (c *Controller) FindAll(){
	
	cursor, err := c.collection.Find(context.TODO(), bson.D{})
	if err != nil {
		log.Fatal(err)
	}

	var files []bson.M
	if err = cursor.All(context.TODO(), &files); err != nil {
		log.Fatal(err)
	}
	fmt.Println(files)
}

func (c *Controller) FindById(id string) bson.D{
	objID, _ := primitive.ObjectIDFromHex(id);
	value := c.collection.FindOne(context.TODO(), bson.M{"_id": objID})

	JSONData := bson.D{}
	err := value.Decode(&JSONData)

	if err != nil {
		panic(err);
	}
	return JSONData;
}



