package file

import (
	"fmt"
	"reflect"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type File struct {
	name string
	path string
}

func New() File {
	var f File;
	return f;
}

func (f *File) SetFromParameters(name, path string) {
	f.name = name;
	f.path = path
}

func (f *File) SetFromBson(obj bson.D) {
	fmt.Println(obj)
}

func (f File) ToMongo () primitive.D{

	elements := bson.D{};

	val := reflect.Indirect(reflect.ValueOf(f))

	for i:=0; i<val.NumField();i++{
		fildName := val.Type().Field(i).Name;
		fildValue := val.FieldByName(fildName).String();
		elements = append(elements, bson.E {Key: fildName, Value: fildValue})
    }

	return elements;

} 
func getField(v reflect.Value, field string) string {
    f := v.FieldByName(field)
    return f.String()
}