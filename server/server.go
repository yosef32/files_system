package server

import (
	"fmt"
	"net"
	"context"
	"romano.com/proto"
	"romano.com/mongo"
	"romano.com/model/file"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

type Server struct {
	port string
}

var mongoManager mongo.Controller;

func New(port string) Server {
	var s Server;
	s.port = port;

	mongoManager = mongo.NewController("mongodb://localhost:27017");
	mongoManager.SetCollection("test", "files")

  	return s;
}

func (s *Server) CreateListener() {

	listener, err := net.Listen("tcp", s.port);
	if err != nil {
		panic(err)
	}

	srv := grpc.NewServer();
	proto.RegisterAddServiceServer(srv, s);
	reflection.Register(srv);

	fmt.Println("GRpc run on" + s.port);
	if e := srv.Serve(listener); e != nil {
		fmt.Println("GRpc not run: ", err.Error());
	}
	
}

func (s *Server) Create(ctx context.Context, request *proto.CreateRequest) (*proto.Response, error) {
	name, path := request.GetName(), request.GetPath();
	f := file.New();
	f.SetFromParameters(name, path);
	id := mongoManager.Insert(f.ToMongo());
	
	return &proto.Response{Result: id}, nil;
}

func (s *Server) Read(ctx context.Context, request *proto.ReadRequest) (*proto.ReadRespons, error) {
	id := request.GetId();
	f := file.New();
	f.SetFromBson(mongoManager.FindById(id));
	return &proto.ReadRespons{Name: ""}, nil;
}

func (s *Server) Update(ctx context.Context, request *proto.UpdateRequest) (*proto.Response, error) {
	id := request.GetId();
	return &proto.Response{Result: id}, nil;
}

func (s *Server) Delete(ctx context.Context, request *proto.DeleteRequest) (*proto.Response, error) {
	id := request.GetId();
	return &proto.Response{Result: id}, nil;
}


